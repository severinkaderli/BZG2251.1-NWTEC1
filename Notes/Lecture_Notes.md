---
title: "BZG2251.1-NWTEC1"
subtitle: "Grundlagen der Physik und Elektrotechnik"
author:
    - Severin Kaderli
extra-info: true
institute: "Berner Fachhochschule"
department: "Technik und Informatik"
lecturer: "Christian Schneeberger"
lang: "de-CH"
...

# Modulinformationen

## Modulunterlagen
[Moodle](https://moodle.bfh.ch/course/view.php?id=18649) (Einschreibeschlüssel ist **BZG2251.1p2018**)

## Bewertung
* 40% - Zwischenprüfung
* 60% - Abschlussprüfung

# SI-Einheiten
Standardisierte Einheiten und Grössordnung für die Nutzung in der Physik.

## Einheiten
* Strecke s [$\si{m}$]
* Masse m [$\si{kg}$]
* Zeit t [$\si{s}$]
* Strom I [$\si{A}$]
* Temperatur T [$\si{K}$]
* Molare Masse [$\si{mol}$]
* Lichtstärke J [$\si{cd}$] 

## Grössenordnung
$$
\begin{aligned}
& 0.000001 &\quad \mu \SI{}{m} &\quad 10^{-6} \\
& 0.001 &\quad \SI{}{mm} &\quad 10^{-3} \\
& 0.01 &\quad \SI{}{cm} &\quad 10^{-2} \\
& 0.1 &\quad \SI{}{dm} &\quad 10^{-1} \\
& 1 &\quad \SI{}{m} &\quad 10^{1} \\
& 1000 &\quad \SI{}{Km} &\quad 10^{3} \\
& 1000000 &\quad \SI{}{Mm} &\quad 10^{6} \\
& 1000000000 &\quad \SI{}{Gm} &\quad 10^{9} \\ 
\end{aligned}
$$

# Einheitsumrechnungen
## Winkel
$$
deg = \frac{rad \cdot 180}{\pi}
$$

$$
rad = \frac{deg \cdot \pi}{180}
$$

## Koordinaten
### Kartesisch zu Polar
$$
\begin{aligned}
    r &= \sqrt{x^{2} + y^{2}} \\
    \phi &= tan^{-1} (\frac{y}{x})
\end{aligned}
$$

### Polar zu Kartesisch
$$
\begin{aligned}
    x &= r \cdot cos(\phi) \\
    y &= r \cdot sin(\phi)
\end{aligned}
$$ 

# Kinematik
Die Kinematik ist die geometrische Beschreibung von bewegten Körpern.

## Strecke
Formelzeichen: $s$  
Einheit: $\SI{}{m}$

Alternative Namen für die Strecke sind *Länge*, *Raum*, *Punkt*, *Ort* oder *Auslenkung*. Als alternatives Formelzeichen wird auch $l$ verwendet.
$$
\Delta s = s_{2} - s_{1}
$$

## Zeit t [s]
Formelzeichen: $t$  
Einheit: $\SI{}{s}$

$$
\Delta t = t_{2} - t_{1}
$$

## Geschwindigkeit
Formelzeichen: $v$  
Einheit: $\SI{}{m/s}$

$$
v = \frac{\Delta s}{\Delta t} = \frac{s_{2} - s_{1}}{t_{2} - t_{1}}
$$

## Gleichförmige Bewegung (Konstante Geschwindigkeit)

$$
s = s_{0} + v * t
$$

## Weg-Zeit-Diagramm

![Weg-Zeit-Diagramm](./assets/wegzeit.png)

## Geschwindigkeits-Zeit-Diagramm 

![Geschwindigkeits-Zeit-Diagramm](./assets/geschwindigkeitszeit.png)

## Beschleunigung
Formelzeichen: $a$  
Einheit: $\SI{}{m/s^{2}}$

$$
\begin{aligned}
a &= \frac{\Delta v}{\Delta t}
\end{aligned}
$$

$$
\begin{aligned}
v = v_{0} + a \cdot t
\end{aligned}
$$

## Gleichmässig Beschleunigte Bewegung

$$
\begin{aligned}
s =& s_{0} + v_{0} \cdot t + \frac{1}{2} a\cdot t^{2} \\
s =& \frac{a \cdot t^{2}}{2}
\end{aligned}
$$

### Freier Fall

Den Freien Fall kann man mit der obigen Formel einfach berechnen. Als Beschleunigung $a$ verwendet man dann jedoch die Gravitationskonstante $g = 9.81m/s^{2}$. Da es sich beim Freien Fall um eine "negative" Beschleunigung handelt, muss man $g​$ auch negativ in die Formel einsetzen.

### Schiefer Wurf

$$
\begin{aligned}
x(t) &= x_{0} + v_{0x} \cdot t + \frac{1}{2}a_{x} \cdot t^{2} = x_{0} + v_{0} \cdot cos(\alpha) \\
y(t) &= y_{0} + v_{0y} \cdot t + \frac{1}{2}a_{y} \cdot t^{2} = y_{0} + v_{0} \cdot sin(\alpha) - \frac{1}{2}g\cdot t^{2}
\end{aligned}
$$

## Gleichförmige Kreisbewegung
### Winkelgeschwindigkeit
Formelzeichen: $\omega$  
Einheit:  $\si{\degree/s}$ oder $\si{1/s}$ 

$$
\omega = \frac{\Delta \phi}{\Delta t}
$$

Für eine ganze Umdrehung: 

$$
\omega = \frac{360\si{\degree}}{T} = \frac{2\pi}{T} \to T = \frac{2\pi}{w}
$$

### Frequenz
Formelzeichen: $f$  
Einheit: $\SI{}{1/s}$ = $\SI{}{Hz}$

$$
f = \frac{1}{T} = \frac{\omega}{2\pi}
$$

### Bahngeschwindigkeit
Formelzeichen: $v$  
Einheit: $\SI{}{m/s^{2}}$

$$
v = r \cdot \omega
$$

# Dynamik
## Newtonsche Axiome
### 1. Axiom: Trägheitsprinzip
Wirkt keine Kraft auf einen Körper, behält er Betrag und Richtung der Geschwindigkeit.

### 2. Axiom: Aktionsprinzip
Wirkt auf einen Körper eine Kraft, so wird er in Richtung der Kraft beschleunigt.

### 3. Axiom: Reaktionsprinzip
Besteht zwischen zwei Körpern A und B eine Kraftwirkung, so ist die Kraft, welche von A auf B ausgeübt wird, der Kraft, die B auf A ausübt entgegengesetzt gleich.

## Kraft
Formelzeichen: $F$  
Einheit: $\SI{}{N}$ oder $\SI{}{kg \cdot m/s^{2}}$

Die Kraft $F$ ist dabei ein Vektor.

$$
F = m \cdot a
$$

Resultierende Kraft:
$$
F_{res} = F_{1} + F_{2} + \dots + F_{n} = \sum^{n}_{i=1}{F_{i}}
$$

Gravitationskraft:
$$
F_{G} = -G \cdot \frac{m_{1}\cdot m_{2}}{r^{2}}
$$

Zentripetalkraft:
$$
F_{z} = m \cdot a_{z} = m \cdot \frac{v^{2}}{r}
$$

Reibungskraft:
$$
F_{R} = \mu \cdot F_{N}
$$

## Impuls
Formelzeichen: $p$  
Einheit: $\SI{}{kg \cdot m/s}$

$$
\begin{aligned}
    p_{1} + p_{2} &= p'_{1} + p'_{2} \\
    m_{1} \cdot v_{1} + m_{2} \cdot 0 &= m_{1} \cdot 0 + m_{2} \\
    v_{2} &= \frac{m_{1}}{m_{2}} \cdot v_{1}
\end{aligned}
$$

Der Gesamtinpuls in einem geschlossenen System ist konstant.

## Energie
Formelzeichen: $E$  
Einheit: $\SI{}{J}$ oder $\SI{}{Nm}$ oder $\SI{}{Ws}$

Arbeit:
$$
W = F \cdot \Delta s
$$

Potentielle Energie:
$$
E_{pot} = m \cdot g \cdot h
$$

Kinetische Energie:
$$
E_{kin} = \frac{1}{2} \cdot m \cdot v^{2}
$$

Die Gesamtenergie in einem geschlossenen System ist konstant.

## Leistung
Formelzeichen: $P$  
Einheit: $\SI{}{W}$

$$
P = \frac{W}{t} 
$$

# Elektrotechnik

## Ladung
Formelzeichen: $Q$ oder $q$  
Einheit: $\SI{}{C}$

Die Elementarladung $e$ beträgt $\SI{1.6e-19}{C}$. 

Die Ladung in einem geschlossenen System ist konstant.

## Coulombkraft
Formelzeichen: $F_{c}$  
Einheit: $\SI{}{N}$

$$
F_{c} = k \cdot \frac{q_{1} \cdot q_{2}}{r^{2}}
$$

$$
K = \frac{1}{4 \pi \epsilon_{0}}
$$

$$
\epsilon_{0} = 8.85 \cdot 10^{-12}
$$


## Elektrisches Feld
Formelzeichen: $E$  
Einheit: $\SI{}{N/C}$

$$
E = \frac{F_{c}}{q}
$$

## Elektrisches Potenzial
Formelzeichen: $\phi$  
Einheit: $\SI{}{N/C \cdot m}$

$$
\phi = \frac{K \cdot Q}{r}
$$

$$
\Delta \phi = \phi_{B} - \phi_{A} = -U
$$

## Spannung
Formelzeichen: $U$  
Einheit: $\SI{}{V}$

$$
U = -\Delta \phi
$$

$$
E_{pot} = Q \cdot U
$$

## Strom
Formelzeichen: $I$  
Einheit: $\SI{}{A}$

$$
I = \frac{\Delta Q}{\Delta t}
$$

## Elektrischer Widerstand
Formelzeichen: $R$  
Einheit: $\SI{}{\Omega}$

$$
R = \phi \cdot \frac{l}{A}
$$

## Ohmsches Gesetz
$$
\begin{aligned}
    U &= R \cdot I \\
    I &= \frac{U}{R} \\
    R &= \frac{U}{I}
\end{aligned}
$$

## Serieschaltung
$$
\begin{aligned}
    R &= R_{1} + R_{2} + \dots + R_{n} \\
    I &= I_{1} = I_{2} = \dots = I_{n} \\
    U &= U_{1} + U_{2} + \dots + U_{n}
\end{aligned}
$$

## Parallelschaltung
$$
\begin{aligned}
    \frac{1}{R} &= \frac{1}{R_{1}} + \frac{1}{R_{2}} + \dots + \frac{1}{R_{n}} \\
    U &= U_{1} = U_{2} = \dots = U_{n} \\
    I &= I_{1} + I_{2} + \dots + I_{n}
\end{aligned}
$$

## Elektrische Leistung
Formelzeichen: $P$  
Einheit: $\SI{}{W}$

$$
P = U \cdot I
$$
